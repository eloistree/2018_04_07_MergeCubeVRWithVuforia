﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetAllTime : MonoBehaviour {

    public Transform _resetToZeroAllTime;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        _resetToZeroAllTime.position = Vector3.zero;
        _resetToZeroAllTime.rotation = Quaternion.identity;

    }
}
