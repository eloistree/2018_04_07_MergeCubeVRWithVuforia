﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

[RequireComponent(typeof(TrackableBehaviour))]
public class MirrorWithLerp : MonoBehaviour, ITrackableEventHandler
{


    public Camera _cameraTracked;
    public Camera _cameraUsed;
    public float _speed = 2;
    public Transform _objectAffected;
    public Vector3 _fromPosition;
    public Vector3 _toPosition;
    public bool _isActif;
    public float _delayToDeactivate = 0.2f;



    public void Update()
    {
        _cameraUsed.transform.position = Vector3.Lerp(_cameraUsed.transform.position, _cameraTracked.transform.position, Time.deltaTime * _speed);
        _cameraUsed.transform.rotation = Quaternion.Lerp(_cameraUsed.transform.rotation, _cameraTracked.transform.rotation, Time.deltaTime * _speed);


    }

    public TrackableBehaviour mTrackableBehaviour;

    // Use this for initialization
    void Start () {
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();

        if (mTrackableBehaviour)
            mTrackableBehaviour.RegisterTrackableEventHandler(this);

    }

    protected virtual void OnDestroy()
    {
        if (mTrackableBehaviour)
            mTrackableBehaviour.UnregisterTrackableEventHandler(this);
    }
    
    public void OnTrackableStateChanged(
        TrackableBehaviour.Status previousStatus,
        TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " found");
            _isActif = true;
            OnTrackingFound();
        }
        else if (previousStatus == TrackableBehaviour.Status.TRACKED &&
                 newStatus == TrackableBehaviour.Status.NOT_FOUND)
        {
            Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " lost");
            _isActif = false;
            OnTrackingLost();
        }
        else
        {
            // For combo of previousStatus=UNKNOWN + newStatus=UNKNOWN|NOT_FOUND
            // Vuforia is starting, but tracking has not been lost or found yet
            // Call OnTrackingLost() to hide the augmentations
            _isActif = false;
            OnTrackingLost();
        }
    }

    protected virtual void OnTrackingLost()
    {

        var rendererComponents = GetComponentsInChildren<Renderer>(true);
        var colliderComponents = GetComponentsInChildren<Collider>(true);
        var canvasComponents = GetComponentsInChildren<Canvas>(true);

        // Disable rendering:
        foreach (var component in rendererComponents)
            component.enabled = false;

        // Disable colliders:
        foreach (var component in colliderComponents)
            component.enabled = false;

        // Disable canvas':
        foreach (var component in canvasComponents)
            component.enabled = false;

        StartCoroutine(InformSpawnCtr());
    }

    protected virtual void OnTrackingFound()
    {
        var rendererComponents = GetComponentsInChildren<Renderer>(true);
        var colliderComponents = GetComponentsInChildren<Collider>(true);
        var canvasComponents = GetComponentsInChildren<Canvas>(true);

        // Enable rendering:
        foreach (var component in rendererComponents)
            component.enabled = true;

        // Enable colliders:
        foreach (var component in colliderComponents)
            component.enabled = true;

        // Enable canvas':
        foreach (var component in canvasComponents)
            component.enabled = true;

        if(lastCoroutine!=null)
            StopCoroutine(lastCoroutine);
        lastCoroutine= StartCoroutine(InformSpawnCtr());
    }
    private Coroutine lastCoroutine;
    private IEnumerator InformSpawnCtr()
    {
        Debug.Log("1");
        if (_isActif)
        {
            _objectAffected.gameObject.SetActive(true);
          
        }
        yield return new WaitForSeconds(_delayToDeactivate);

        Debug.Log("2:"+ _isActif);

        if (!_isActif)
        {

            _objectAffected.gameObject.SetActive(false);
        }
    }
}
